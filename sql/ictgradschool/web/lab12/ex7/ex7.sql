
DROP TABLE IF EXISTS player;
CREATE TABLE player(
  player_name VARCHAR(50),
  player_number int,
  age int,
  nationality VARCHAR(30),
  primary key (player_number)
);

  INSERT INTO player (player_name, player_number, age, nationality)
  VALUES  ('IronMan', 1, 42, 'US'),
('Black Widow', 2, 39, 'US'),
('Hulk', 3, 45, 'US'),
('Spiderman', 4, 17, 'US'),
('Star Lord', 5, 30, 'US'),
('Thor', 6, 99, 'US'),
('Captain America', 7, 99, 'US'),
('Batman', 8, 35,  'US'),
('Nightwing',9, 27,  'US'),
('Batgirl', 10, 24,'US'),
('Matron', 11, 32,  'US'),
('Black Canary', 12, 25, 'US'),
('Wonder Woman', 13, 89, 'US'),
('Aquaman', 14, 45, 'US');


DROP TABLE IF EXISTS team;

CREATE TABLE team(
team_number int NOT NULL AUTO_INCREMENT,
  team_name VARCHAR(50),
  captain VARCHAR(50),
  home_city VARCHAR(50),
  points_scored int,
  primary key (team_number)
#   foreign key (team_number)
);

INSERT INTO team (team_name, captain, home_city, points_scored)
VALUES ('Smashing Pumpkins', 'Ironman', 'New York', 999),
       ('Gotham Ghouls', 'Batman', 'Gothman City', 998);


DROP TABLE IF EXISTS combined;

CREATE TABLE combined(
  team_number int NOT NULL,
  player_number int NOT NULL,
  PRIMARY KEY (team_number, player_number),
  FOREIGN KEY (player_number) REFERENCES player(player_number),
  FOREIGN KEY (team_number) REFERENCES team(team_number)
);

INSERT INTO combined (team_number, player_number)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (1, 5),
       (1, 6),
       (1, 7),
       (2, 8),
       (2, 9),
       (2, 10),
       (2, 11),
       (2, 12),
       (2, 13),
       (2, 14);