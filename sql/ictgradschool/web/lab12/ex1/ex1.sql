-- Answers to exercise 1 questions
    SELECT dept FROM unidb_courses;

  SELECT semester FROM unidb_attend;

  SELECT dept, num, descrip FROM unidb_courses;

  SELECT fname, lname, country FROM unidb_students ORDER BY fname;

  SELECT CONCAT(student.fname, ' ', student.lname) AS student_name,
         CONCAT(mentor.fname, ' ', mentor.lname) AS mentor_name
    FROM unidb_students AS student,
         unidb_students AS mentor
    WHERE student.mentor = mentor.id
    ORDER BY mentor.lname;

SELECT fname, lname FROM unidb_lecturers ORDER BY office;

SELECT fname, lname FROM unidb_lecturers WHERE staff_no>500;

SELECT fname, lname FROM unidb_students WHERE id>1668 AND id<1824;

SELECT fname, lname, country FROM unidb_students WHERE country != 'MX' AND country !='FR' AND country !='SC';

SELECT fname, lname, office FROM unidb_lecturers WHERE office!='FG.1.01' AND office!='L.2.34' AND office!='L.3.13';

SELECT dept, num, descrip FROM unidb_courses WHERE dept!='comp';

SELECT fname, lname FROM unidb_students WHERE country = 'MX' OR country='FR';