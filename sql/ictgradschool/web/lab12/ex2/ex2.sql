-- Answers to exercise 2 questions
SELECT s.fname, s.lname FROM unidb_students AS s, unidb_attend AS a
                             WHERE s.id = a.id AND a.dept = 'comp' AND a.num ='219';

SELECT DISTINCT mentor.fname, mentor.lname
FROM unidb_students AS student, unidb_students AS mentor
                             WHERE
    student.mentor = mentor.id AND mentor.country!='NZ';

SELECT lecturer.fname, lecturer.lname
FROM unidb_lecturers AS lecturer, unidb_teach AS teach
                            WHERE
lecturer.staff_no = teach.staff_no AND teach.num='219';

SELECT student.fname, student.lname
FROM unidb_students AS student, unidb_attend AS attend
WHERE attend.id=student.id AND attend.num='258';

SELECT CONCAT(student.fname, ' ', student.lname) AS student_name,
       CONCAT(mentor.fname, ' ', mentor.lname) AS mentor_name
FROM unidb_students AS student,
     unidb_students AS mentor
WHERE student.mentor = mentor.id
ORDER BY mentor.lname;

SELECT DISTINCT CONCAT(lecturer.fname, ' ', lecturer.lname) AS lecturer_name,
        CONCAT(student.fname, ' ', student.lname) AS student_name
FROM unidb_lecturers as lecturer,
     unidb_students as student
WHERE lecturer.office LIKE 'G%'
AND student.country!='NZ'
ORDER BY lecturer.fname, lecturer.lname;

SELECT DISTINCT CONCAT(course.dept, ' ', course.num) AS course_name,
                CONCAT(student.fname, ' ', student.lname) AS student_name
                FROM unidb_courses as course,
                     unidb_students as student
WHERE course.rep_id = student.id AND course.dept='comp' AND course.num='219';